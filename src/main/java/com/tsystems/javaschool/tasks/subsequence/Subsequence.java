package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        try{
            int xl = x.size();
            int yl = y.size();
            int j = 0;
            // Проверяем для каждого объекта списка 'x', содержится ли он в списке 'y'
            for (int i = 0; i < yl && j < xl; i++){
                if (x.get(j).equals(y.get(i))){
                    j++;
                }
            }
            // Сравниваем количество объектов списка 'x', содержащихся в списке 'y',
            // с данным количеством объектов в списке 'x'
            return (j == xl);
        }catch (NullPointerException e){
            throw new IllegalArgumentException();
        }

    }
}
