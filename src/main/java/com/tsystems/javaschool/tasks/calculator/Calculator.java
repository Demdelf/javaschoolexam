package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        try{
            char[] tokens = statement.toCharArray();

            // Stack для чисел: 'values'
            Stack<Double> values = new Stack<>();

            // Stack для операторов: 'ops'
            Stack<Character> ops = new Stack<>();

            for (int i = 0; i < tokens.length; i++)
            {
                // Текущий токен - пробел, пропускаем его
                if (tokens[i] == ' ')
                    continue;

                // Текущий токен - число, пушим в стек для чисел
                if ((tokens[i] >= '0' && tokens[i] <= '9') || tokens[i] =='.')
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    // Может быть несколько цифр в числе, число может быть дробным
                    while (i < tokens.length && ((tokens[i] >= '0' && tokens[i] <= '9') || tokens[i] =='.')){
                        stringBuilder.append(tokens[i]);
                        i++;
                    }
                    if(i == tokens.length) i--;

                    values.push(Double.parseDouble(stringBuilder.toString()));
                }
                // Текущий токен - запятая, возвращаем null
                if (tokens[i] == ',')
                    return null;
                // Текущий токен - открывание скобки, пушим в стек для операторов
                if (tokens[i] == '(')
                    ops.push(tokens[i]);

                // Обнаружена закрывающая скобка, вычисляем выражение внутри скобок
                if (tokens[i] == ')')
                {
                    while (ops.peek() != '(')
                        values.push(applyOp(ops.pop(), values.pop(), values.pop()));
                    ops.pop();
                }

                // Текущий токен - оператор.
                if (tokens[i] == '+' || tokens[i] == '-' ||
                        tokens[i] == '*' || tokens[i] == '/')
                {
                   //Пока вершина 'ops' имеет такой же или больший приоритет перед текущим токеном,
                    //являющимся оператором. Применить верхний оператор из 'ops' к двум верхним элементам в стеке чисел

                    while (!ops.empty() && hasPrecedence(tokens[i], ops.peek()))
                        values.push(applyOp(ops.pop(), values.pop(), values.pop()));

                    // Пушим текущий токен в 'ops'.
                    ops.push(tokens[i]);
                }
            }

            //К этому моменту выражение 'statement' уже полностью спарсено.
            //Осталось применить оставшиеся операторы к оставшимся значениям
            while (!ops.empty())
                values.push(applyOp(ops.pop(), values.pop(), values.pop()));

            // Вершина 'values' содержит результат исходного выражения, возвращаем его
            double result = values.pop();

            // Если результат целое число - выводим без дробной части
            if (result % 1 == 0) return String.valueOf((int)result);
            return String.valueOf(result);
        }catch (UnsupportedOperationException | EmptyStackException | NullPointerException | NumberFormatException e){
            //Если поймали перечисленные ошибки - выводим null
            return null;
        }

    }

    // Возвращает true, если 'op2' has higher or same precedence as 'op1'
    // иначе возвращаем false.
    public static boolean hasPrecedence(char op1, char op2)
    {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
            return false;
        else
            return true;
    }

    // Служебный метод для применения оператора 'op' к операндам 'a' и 'b'.
    // Возвращает результат.
    public static double applyOp(char op, double b, double a)
    {
        switch (op)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0)
                    throw new
                            UnsupportedOperationException("Cannot divide by zero");
                return a / b;
        }
        return 0;
    }

}
