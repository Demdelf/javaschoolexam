package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {


    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */


    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try{
            // Сортируем данный список с помощью параллельного стрима
            inputNumbers = inputNumbers.parallelStream().sorted().collect(Collectors.toList());
            // Кол-во чисел в списке
            int n = inputNumbers.size();

            int idx = 0;
            int numRows = 0;
            int numColumns = 0;
            int[][] result;

            // Считаем количество строк в пирамиде
            while(idx < n){
                numRows++;
                for(int numInRow = 0; numInRow < numRows; numInRow++){
                    idx++;
                }
            }

            // Проверяем возможность построения пирамиды из данного количества чисел, иначе кидаем ошибку
            if (n != sumOfDigitsFrom1ToN(numRows)) throw new CannotBuildPyramidException();

            // Считаем количество столбцов в пирамиде
            numColumns = numRows*2 - 1;

            result = new int[numRows][numColumns];

            //Заполняем матрицу
            idx = 0;
            int q = 0;
            for(int i = 1; i <= numRows && idx < n; i++){
                for(int j = 0; j < (numRows - i) ; j++){
                    result[i - 1][q] = 0;
                    q++;
                }

                for(int j = 0; j < i; j++){
                    result[i - 1][q]  = inputNumbers.get(idx);
                    q++;
                    if(q < numColumns) {
                        result[i - 1][q]  = 0;
                        q++;
                    }
                    idx++;
                    if(idx >= n){
                        break;
                    }
                }
                while (q < numColumns){
                    result[i - 1][q]  = 0;
                    q++;
                }
                q = 0;
            }
            for (int i = 0; i < numRows; i++) {
                for (int j = 0; j < numColumns; j++) {
                    System.out.print(result[i][j] + "\t");
                }
                System.out.println();
            }
            return result;

        }catch (NullPointerException | IllegalArgumentException e){
            throw new CannotBuildPyramidException();
        }

    }
    // Возвращает сумму всех целых чисел
    // от 1 до n
    public int sumOfDigitsFrom1ToN(int n)
    {
        int result = 0;
        for (int x = 1; x <= n; x++)
            result += x;

        return result;
    }
}
